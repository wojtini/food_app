package com.example.food_order_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.example.food_order_app.adapter.FoodMenuAdapter;
import com.example.food_order_app.model.FoodMenu;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView foodRecycler;
    FoodMenuAdapter foodMenuAdapter;
    List<FoodMenu> foodMenuList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_screen);


        foodMenuList.add(new FoodMenu("Sushi","15",R.drawable.image));
        foodMenuList.add(new FoodMenu("Ramen","20",R.drawable.image_2));
        foodMenuList.add(new FoodMenu("Sashimi","18",R.drawable.image_3));
        foodMenuList.add(new FoodMenu("Tempura","22",R.drawable.image_4));
        foodMenuList.add(new FoodMenu("Soba","12",R.drawable.image_5));
        foodMenuList.add(new FoodMenu("Udon","30",R.drawable.image_6));
        foodMenuList.add(new FoodMenu("Onigiri","10",R.drawable.image_7));
        foodMenuList.add(new FoodMenu("Yakitori","20",R.drawable.image_8));
        foodMenuList.add(new FoodMenu("Sukiyaki","18",R.drawable.image_9));
        foodMenuList.add(new FoodMenu("Oden","22",R.drawable.image_10));
        foodMenuList.add(new FoodMenu("Miso Soup","18",R.drawable.image_11));

        setFoodRecycler(foodMenuList);

        EditText search_txt = findViewById(R.id.search_text);
        search_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());

            }
        });
    }


    public void goToShoppingCart(View view) {
        Intent intent = new Intent(MainActivity.this, ShoppingCart.class);
        startActivity(intent);
    }
    private void filter(String text)
    {
        ArrayList<FoodMenu> filteredlist = new ArrayList<>();

        for(FoodMenu item: foodMenuList)
        {
            if(item.getName().toLowerCase().contains(text.toLowerCase()))
            {
                filteredlist.add(item);
            }
        }
        foodMenuAdapter.filterList(filteredlist);

    }

    private void setFoodRecycler(List<FoodMenu> foodMenuList)
    {
        foodRecycler = findViewById(R.id.recycler_id);
        RecyclerView.LayoutManager layoutManager  = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        foodRecycler.setLayoutManager(layoutManager);
        foodMenuAdapter = new FoodMenuAdapter(this,foodMenuList);
        foodRecycler.setAdapter(foodMenuAdapter);
    }

}