package com.example.food_order_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.food_order_app.FoodDetails;
import com.example.food_order_app.R;
import com.example.food_order_app.model.FoodMenu;

import java.util.ArrayList;
import java.util.List;


public class FoodMenuAdapter  extends RecyclerView.Adapter<FoodMenuAdapter.FoodMenuViewHolder> {

    Context context;
    List<FoodMenu> foodMenusList;

    public FoodMenuAdapter(Context context, List<FoodMenu> foodMenuList) {
        this.context = context;
        this.foodMenusList = foodMenuList;
    }

    @NonNull
    @Override
    public FoodMenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.food_menu_row_item,parent,false);
        return new FoodMenuViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull FoodMenuViewHolder holder, int position) {

        holder.foodImage.setImageResource((foodMenusList.get(position).getImageURL()));
        holder.name.setText(foodMenusList.get(position).getName());
        holder.price.setText(foodMenusList.get(position).getPrice());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, FoodDetails.class);
                i.putExtra("name", foodMenusList.get(position).getName());
                i.putExtra("price", foodMenusList.get(position).getPrice());
                i.putExtra("image", foodMenusList.get(position).getImageURL());

                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return foodMenusList.size();
    }

    public void filterList (ArrayList<FoodMenu> filteredList)
    {
        foodMenusList = filteredList;
        notifyDataSetChanged();
    }

    public static final class FoodMenuViewHolder extends RecyclerView.ViewHolder{

        ImageView foodImage;
        TextView price,name;


        public FoodMenuViewHolder(@NonNull View itemView) {
            super(itemView);

            foodImage = itemView.findViewById(R.id.food_Image);
            price = itemView.findViewById(R.id.food_price);
            name = itemView.findViewById(R.id.food_name);
        }
    }
}
