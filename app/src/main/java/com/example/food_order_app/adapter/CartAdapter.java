package com.example.food_order_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.food_order_app.R;
import java.util.ArrayList;
import java.util.List;

class CartProductAdapter extends RecyclerView.Adapter<CartProductAdapter.CartViewHolder> {
    List<CartProductAdapter>carts;
    Context context;

    public CartProductAdapter( Context context,List<CartProductAdapter> carts) {
        this.carts = carts;
        this.context = context;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item,parent,false);
        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, final int position) {
        final CartProductAdapter cart=carts.get(position);



        holder.deletbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carts.remove(position);
                notifyDataSetChanged();
                Intent intent=new Intent("mymsg");

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return carts.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder{
        public View deletbtn;
        ImageView foodImage;
        TextView fixprice,name;

        public CartViewHolder(@NonNull View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.txtname);
            fixprice=(TextView)itemView.findViewById(R.id.food_price);
            foodImage=itemView.findViewById(R.id.food_Image);

        }
    }
}