package com.example.food_order_app.model;

public class  FoodMenu {

    String name;
    String price;
    Integer imageURL;

    public FoodMenu(String name, String price, Integer imageURL) {
        this.name = name;
        this.price = price;
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public Integer getImageURL() {
        return imageURL;
    }
}
