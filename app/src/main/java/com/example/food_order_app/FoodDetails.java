package com.example.food_order_app;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class FoodDetails extends AppCompatActivity {



    ImageView imageView;
    TextView itemName,itemPrice,how_much;


    String name, price, basic_price;
    Integer imageUrl ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_detail_screen);

        Intent intent = getIntent();

        name = intent.getStringExtra("name");
        price = intent.getStringExtra("price");
        imageUrl = intent.getIntExtra("image",0);

        imageView = findViewById(R.id.obraz_dania);
        itemName = findViewById(R.id.nazwa_dania);
        itemPrice = findViewById(R.id.cena_dania);
        how_much = findViewById(R.id.how_much);



        imageView.setImageResource(imageUrl);
        itemName.setText(name);
        itemPrice.setText(price);
        basic_price=itemPrice.getText().toString();



    }



    public void add(View view)
    {
        int amount,price_sum,basic_pr;
        amount = Integer.parseInt(how_much.getText().toString());
        price_sum = Integer.parseInt(itemPrice.getText().toString());
        basic_pr=Integer.parseInt(basic_price);

        if(amount<20)
        {
            amount++;
            price_sum =  basic_pr * amount;
        }

        how_much.setText(Integer.toString(amount));
        itemPrice.setText(Integer.toString(price_sum));
    }

    public void goToShoppingCart2(View view) {
        Intent intent = new Intent(FoodDetails.this, ShoppingCart.class);
        startActivity(intent);
    }

    public void sub(View view)
    {
        int amount,price_sum,basic_pr;
        amount = Integer.parseInt(how_much.getText().toString());
        price_sum = Integer.parseInt(itemPrice.getText().toString());
        basic_pr=Integer.parseInt(basic_price);

        if(amount>1)
        {
            amount--;
            price_sum = basic_pr * amount;
        }
        how_much.setText(Integer.toString(amount));
        itemPrice.setText(Integer.toString(price_sum));
    }

}