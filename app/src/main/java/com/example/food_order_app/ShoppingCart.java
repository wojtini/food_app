package com.example.food_order_app;

import android.os.Bundle;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.food_order_app.adapter.FoodMenuAdapter;
import com.example.food_order_app.model.FoodMenu;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart extends AppCompatActivity {
    RecyclerView cartRecycler;

    List<ShoppingCart> shoppingCartList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart);


    }
    public void goToMain(View view) {
        Intent intent = new Intent(ShoppingCart.this, MainActivity.class);
        startActivity(intent);
    }
}